<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:ti="http://chs.harvard.edu/xmlns/cts">
    <xsl:strip-space elements="*"/>

    <xsl:template match="@* | node()">
        <xsl:copy copy-namespaces="yes">
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>


    <xsl:template match="/">
        <!--Métadonnées-->
        <xsl:for-each select="tei:teiCorpus[tei:teiCorpus]">
            <xsl:result-document
                href="/home/mgl/Bureau/programmes/Nautilus/tests/testing_data/politLit/data/{@xml:id}/__cts__.xml">
                <ti:textgroup xsl:exclude-result-prefixes="tei" projid="politLit:{@xml:id}"
                    urn="urn:dts:politLit:{@xml:id}">
                    <ti:groupname xml:lang="{@xml:lang}">
                        <xsl:value-of select="tei:teiHeader//tei:title"/>
                    </ti:groupname>
                </ti:textgroup>
            </xsl:result-document>
        </xsl:for-each>

        <xsl:for-each select="descendant::tei:teiCorpus[parent::tei:teiCorpus]">
            <xsl:variable name="textgroup">
                <xsl:value-of select="parent::tei:teiCorpus/@xml:id"/>
            </xsl:variable>
            <xsl:result-document
                href="/home/mgl/Bureau/programmes/Nautilus/tests/testing_data/politLit/data/{$textgroup}/{@xml:id}/__cts__.xml">
                <ti:work xsl:exclude-result-prefixes="tei" groupUrn="urn:dts:politLit:{$textgroup}"
                    urn="urn:dts:politLit:{$textgroup}.{@xml:id}">
                    <ti:title xml:lang="lat">
                        <xsl:value-of select="tei:teiHeader//tei:title"/>
                    </ti:title>
                    <xsl:for-each select="tei:TEI[@type = 'transcription']">
                        <xsl:variable name="work">
                            <xsl:value-of select="parent::tei:teiCorpus/@xml:id"/>
                        </xsl:variable>
                        <xsl:variable name="edition">
                            <xsl:value-of select="lower-case(@xml:id)"/>
                        </xsl:variable>
                        <ti:edition workUrn="urn:dts:politLit:{$textgroup}.{$work}"
                            urn="urn:dts:politLit:{$textgroup}.{$work}.{$edition}">
                            <ti:label xml:lang="eng">Témoin <xsl:value-of
                                    select="descendant::tei:witness/@xml:id"/></ti:label>
                            <ti:description xml:lang="lat">
                                <xsl:value-of select="descendant::tei:witness/text()"/>
                            </ti:description>
                        </ti:edition>
                    </xsl:for-each>
                </ti:work>
            </xsl:result-document>
        </xsl:for-each>
        <!--Métadonnées-->


        <!--Textes-->
        <xsl:for-each select="descendant::tei:TEI[@type = 'transcription']">
            <xsl:variable name="workgroup">
                <xsl:value-of select="ancestor::tei:teiCorpus[child::tei:teiCorpus]/@xml:id"/>
            </xsl:variable>
            <xsl:variable name="work">
                <xsl:value-of select="ancestor::tei:teiCorpus[not(child::tei:teiCorpus)]/@xml:id"/>
            </xsl:variable>
            <xsl:variable name="edition">
                <xsl:value-of select="lower-case(@xml:id)"/>
            </xsl:variable>
            <xsl:result-document
                href="/home/mgl/Bureau/programmes/Nautilus/tests/testing_data/politLit/data/{$workgroup}/{$work}/{$workgroup}.{$work}.{$edition}.xml">
                <xsl:element name="TEI" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="type">transcription</xsl:attribute>
                    <xsl:attribute name="xml:id" select="@xml:id"/>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>
    <!--Textes-->



</xsl:stylesheet>
