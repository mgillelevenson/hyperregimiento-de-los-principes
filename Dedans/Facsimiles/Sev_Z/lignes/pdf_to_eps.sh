#!/bin/sh

## Script qui permet de convertir tous les fichiers pdf en eps. 
## Pour tout n dans le dossier donné, aller chercher le chemin de n, retirer l'extension, convertir en eps. 

for n in chapitre_*/*/*.pdf; do filename="${n##.*/}" ; filename2="${filename%.*}"; convert $n
 $filename2.eps; done
