### Fonctionnement de ce dossier


Ce dossier contient des captures des graphèmes correspondant aux abréviations utilisées par les copistes. Les images sont triées par manuscrit. 


+ Je n'ajoute ici que les graphèmes et ne fais aucune référence aux abréviations auxquelles ils correspondent. Ainsi le graphème nommé "grond" (image grond.png) peut-il correspondre à l'abréviation de "-guno-" comme de "-gno-". Cette information est donnée dans la description des abréviations que je fais dans ma DTD. 
