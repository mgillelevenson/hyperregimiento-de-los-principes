<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs" version="2.0">


    <xsl:output method="xml"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="@* | node()">
        <xsl:copy copy-namespaces="no">
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>


    <xsl:template match="/">
        <xsl:result-document
            href="/home/mgl/Bureau/These/Edition/hyperregimiento-de-los-principes/Dedans/XML/temoins/castillan/Mad_A.xml">
            <xsl:apply-templates/>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="//tei:publicationStmt/tei:p">
        <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">Produit automatiquement depuis un
            fichier original avec entités. Version normalisée.</xsl:element>
    </xsl:template>

    <xsl:template match="tei:graphic[ancestor::tei:facsimile]">
        <!--Ici on va re-créer les élements tei:graphic en faisant pointer vers la ligne du document d'origine (résultat de l'OCR)-->
        <xsl:element name="graphic" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="url">
                <xsl:value-of select="concat('../', @url)"/>
            </xsl:attribute>
            <xsl:attribute name="copyOf">
                <xsl:value-of
                    select="concat('file:/home/mgl/Bureau/These/Edition/hyperregimiento-de-los-principes/Dedans/XML/analyse_linguistique/Mad_A.xml', parent::tei:surface/@corresp)"
                />
            </xsl:attribute>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:space[@ana = '#tokenisation']">
        <xsl:text> </xsl:text>
    </xsl:template>



    <xsl:template match="tei:lb[@xml:id]">
        <xsl:element name="lb" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="break" select="@break"/>
            <xsl:attribute name="facs">
                <xsl:value-of select="@facs"/>
            </xsl:attribute>
            <xsl:attribute name="xml:id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:attribute name="copyOf">
                <xsl:value-of
                    select="concat('file:/home/mgl/Bureau/These/Edition/hyperregimiento-de-los-principes/Dedans/XML/analyse_linguistique/Mad_A.xml', @facs)"
                />
            </xsl:attribute>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:orig"/>
    <xsl:template match="tei:corr">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:expan">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:reg">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:ex">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="tei:choice">
        <xsl:apply-templates select="tei:reg"/>
        <xsl:apply-templates select="tei:expan"/>
        <xsl:apply-templates select="tei:corr"/>
    </xsl:template>


</xsl:stylesheet>
