# License pour les textes de l'édition. 


## Transcriptions et textes de l'édition

Les textes de cette édition (dont la racine  `TEI` contient un attribut `@type` de valeur 'transcription' ou 'edition')
sont à disposition de quiconque dont le but final n'est pas lucratif: 
ils sont distribués sous licence [CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/). 
En cas de modification, les documents produits doivent être diffusés avec la même license. 

Je **recommande** de conserver l'architecture de l'édition (Un document qui en contient plusieurs différents). 
Dans le cas où ce ne serait pas le cas, et où une transcription serait récupérée individuellement et réutilisée, 
je donne license pour la réutilisation de ces documents **si et seulement si** l'élément ``revisionDesc`` 
du ``teiHeader`` de l'élément ``TEI`` principal ([ici](corpus/revision_desc.xml)) est intégré au document extrait, 
et si la présente license est intégrée au projet en cours (clause *share alike*). 

En cas de modifications, celles-ci doivent être clairement indiquées d'une façon ou d'une autre. 

## Document de thèse

Le document de thèse *au format xml* (`these.xml`) est distribué sous license CC 
[BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode), 
qui devrait autoriser la citation d'extraits de ce travail.
Il ne peut cependant en aucun cas être modifié et publié par une autre personne que son auteur sans autorisation. 

Le document de thèse au format pdf obéit à des contraintes légales et est soumis à une autre licence à préciser.