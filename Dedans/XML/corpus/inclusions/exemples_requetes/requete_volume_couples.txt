
let $wit_1:="#Phil_U"
return
//tei:div[@type='chapitre']/descendant::tei:app[contains(@ana,'#lexicale')]
[descendant::tei:rdgGrp
[count(tokenize(string-join(descendant::tei:rdg/@wit),'#'))=3]
[contains(string-join(descendant::tei:rdg/@wit), $wit_1)]
]/replace(string-join(descendant::tei:rdgGrp[contains(string-join(descendant::tei:rdg/@wit), $wit_1)]/descendant::tei:rdg/@wit), $wit_1, '')
