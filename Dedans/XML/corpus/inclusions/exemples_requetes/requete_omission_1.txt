let $wit_1:="#Esc_Q", $wit_2:="Mad_B" 
return 
//tei:witEnd
[contains(@corresp, $wit_1)]
[not(contains(@corresp, $wit_2))]