<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:sch="http://purl.oclc.org/dsdl/schematron"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
    xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">

    <!--Cette feuille de style produit un document schematron destiné à valider des documents XML-TEI à partir d'un ODD: elle en extrait toutes les règles schematron. Elle est produite à part pour permettre aisément l'activation de fonctions xpath 2.0.-->
    <xsl:output method="xml"/>
    <xsl:strip-space elements="*"/>


    <xsl:template match="/">
        <xsl:element name="sch:schema" namespace="http://purl.oclc.org/dsdl/schematron">
            <xsl:namespace name="sch">http://purl.oclc.org/dsdl/schematron</xsl:namespace>
            <xsl:namespace name="tei">http://www.tei-c.org/ns/1.0</xsl:namespace>
            <xsl:attribute name="queryBinding">xslt2</xsl:attribute>
            <!--https://stackoverflow.com/a/16340266-->
            <xsl:element name="sch:ns" namespace="http://purl.oclc.org/dsdl/schematron">
                <xsl:attribute name="uri">http://www.tei-c.org/ns/1.0</xsl:attribute>
                <xsl:attribute name="prefix">tei</xsl:attribute>
            </xsl:element>
            <xsl:apply-templates select="descendant::tei:div1[@type = 'Encodage']"/>
        </xsl:element>
    </xsl:template>

    <xsl:template
        match="tei:div1[not(@type = 'Encodage')] | tei:teiHeader | tei:head | tei:attList | tei:exemplum"/>

    <xsl:template match="tei:constraintSpec">
        <xsl:element name="sch:pattern" namespace="http://purl.oclc.org/dsdl/schematron">
            <xsl:element name="sch:title" namespace="http://purl.oclc.org/dsdl/schematron">
                <xsl:value-of select="translate(@ident, '_', ' ')"/>
            </xsl:element>
            <xsl:copy-of select="descendant::tei:constraint/descendant::sch:rule"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:moduleRef">
        <xsl:comment/>
        <xsl:comment><xsl:value-of select="@key"/></xsl:comment>
        <xsl:comment/>
    </xsl:template>


    <xsl:template match="tei:desc">
        <xsl:comment><xsl:apply-templates/></xsl:comment>
    </xsl:template>

</xsl:stylesheet>
