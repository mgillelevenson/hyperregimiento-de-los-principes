# Édition comparative du *Regimiento de los prínçipes glosado* B, Livre III, Partie 3

Ce dépôt rassemble l'ensemble des fichiers qui constituent mon travail de thèse 
consacré à l'édition et à l'étude de la dernière partie de la version B de la 
traduction glosée au *De regimine principum* de Gille de Rome, 
traduction glosée de la seconde moitié du XIVe siècle, probablement 
recomposée à la fin de ce même siècle. 
Il comprend les fichiers de l'édition, qui suivent la norme du standard XML-TEI (P5), 
ainsi que les documents de thèse, eux aussi intégralement écrits en XML-TEI.


### Fichiers importants

Le document principal est [corpus.xml](Dedans/XML/corpus/corpus.xml). Il renvoie à tous les fichiers ayant permis de construire cette thèse, à l'aide de XInclude.

#### Texte

- Les transcriptions individuelles se trouvent [ici](Dedans/XML/temoins/castillan)
- Les transcriptions en sortie d'HTR (structurées et annotées avec des entités XML), [ici](Dedans/XML/analyse_linguistique)
- Les fichiers en sortie de chaîne de collation, [ici](Dedans/XML/edition)


#### Documentation

- Trois ODD pour les gouverner tous: la documentation de mon travail est rassemblée dans trois fichiers distincts 
 (un schéma pour les transcriptions, un schéma pour l'édition et 
 un schéma pour les documents de thèse) accessibles
[ici](Dedans/XML/schemas/). Le dossier en question contient aussi des DTD permettant la création d'entités XML; on y trouvera les sorties HTML et RNG des ODD.



### Chemins

Afin de garantir les chemins d'accès aux fichiers (les chemins étant en partie relatifs, et en partie absolus et adaptés à mon propre système de fichiers), je recommande de remplacer toutes les chaînes de caractères
`/home/mgl/Bureau/These/Edition/` par le chemin absolu vers le clone du dépôt sur votre machine. Tout étant encodé en XML-TEI, une recherche dans les noeuds `tei:graphic` permettra de faciliter le remplacement automatique.